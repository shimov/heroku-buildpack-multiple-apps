# Heroku Buildpack: Multiple Apps

Manage multiple Heroku apps within a single Git repository.

Usage
-----

Your Git repository needs to have the following directory structure:

    /
    ├── apps
    │   ├── app1
    │   │   ├── Procfile
    │   │   └── ...
    │   ├── app2
    │   │   ├── Procfile
    │   │   └── ...
    │   └── ...
    └── ...

The directories `app1`, `app2`, ... contain your individual Heroku apps.

For each Heroku app, you need to set the config var `APP` to the correct name:

For `app1`:

    $ heroku config:set APP=app1

For `app2`:

    $ heroku config:set APP=app2

...

Add this buildpack at the first position in the order of buildpack execution:

    $ heroku buildpacks:add --index 1 --app <app> https://github.com/deviantbits/heroku-buildpack-multiple-apps

You can then deploy your Heroku apps as usual:

    $ git push git@heroku.com:<app> master
